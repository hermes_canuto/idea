from django.db import models


# Create your models here.

class Contato(models.Model):
    name = models.CharField(verbose_name="Nome", max_length=200)
    email = models.EmailField(verbose_name="Email", max_length=200)
    assunto = models.CharField(verbose_name="Assunto", max_length=30)
    mensagem = models.TextField(verbose_name="Mensagem", blank=True)
    created_at = models.DateTimeField(verbose_name='Criado em', auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Contato"
        verbose_name_plural = "Contatos"
        ordering = ['id', 'name']
        db_table = 'tb_contact'
