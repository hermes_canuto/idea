from django.contrib import admin

from forum.models import Topic, ReplyPost


# Register your models here.


class ReplyPostnline(admin.TabularInline):
    model = ReplyPost
    extra = 0
    fields = ('creator', 'description',)


class TopicAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'creator', 'created_at']
    list_display_links = ('id', 'title', 'creator')
    search_fields = ['creator', 'title', ]
    inlines = [ReplyPostnline, ]


admin.site.register(Topic, TopicAdmin)
