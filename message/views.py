from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render
from django.utils.html import strip_tags
from django.views.decorators.csrf import csrf_exempt

from ideia.decorators import islogado
from message.models import Message as chat
from project.models import ProjectUser, Project
from project.views import getprojeto
from user.models import User
from user.views import getuser


# Create your views here.


@islogado
def mensagem(request, id_projeto):
    user = getuser(request)
    projeto = getprojeto(request, id_projeto)
    lista = ProjectUser.objects.filter(project=projeto)
    followers = lista.count()
    admin = False
    follower = False
    c = ProjectUser.objects.filter(user=user).filter(project=projeto)
    for i in c:
        # print(i.user, i.project, i.admin)
        admin = i.admin
        follower = True
    ctx = dict({
        'project': projeto,
        'usuario': user,
        'id_project': id_projeto,
        "admin": admin,
        "followers": lista,
        "follower": follower

    })
    return render(request, 'logado/mensagem.html', ctx)


@csrf_exempt
def sendmsg(request):
    request.POST = request.POST.copy()
    for item in request.POST.dict():
        request.POST[item] = strip_tags(request.POST[item])

    if request.method == 'POST' and 'msg' in request.POST:
        msg = request.POST['msg']
        if msg:
            sender = User.objects.get(id=request.POST['sender'])
            to = User.objects.get(id=request.POST['to'])
            project = Project.objects.get(id=request.POST['id_project'])
            sv = chat(
                sender=sender,
                receiver=to,
                project=project,
                message=msg)
            sv.save()
            rt = dict({
                "message": sv.message,
                "created_at": "{} {}".format(str(format(sv.created_at, "%d/%m/%Y")) ,  str(sv.created_at)[11:19])
            })
            return HttpResponse(JsonResponse(rt), content_type="application/json")
    return HttpResponse("erro")
