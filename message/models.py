from django.db import models

from project.models import Project
from user.models import User


# Create your models here.


class Message(models.Model):
    sender = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Remetente",
                               related_name="mensagem_remetente")
    receiver = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Destinatário",
                                 related_name="mensagem_destinariario")
    project = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name="Projeto",
                                related_name="Mensagem_projeto")
    message = models.TextField(verbose_name="Mensagem", null=True)
    read = models.BooleanField(verbose_name='Já foi lida', default=False)
    created_at = models.DateTimeField(verbose_name='Criado em', auto_now=True)

    def __str__(self):
        return self.message

    class Meta:
        verbose_name = "Mensagem"
        verbose_name_plural = "mensagens"
        ordering = ['created_at']
        db_table = 'tb_message'
