from django.db import models

from user.models import Interests, User


def setdata(item, followers, follower, admin):
    return ({"project": {"id": item.id,
                         "name": item.name,
                         "resume": item.resume,
                         "need": item.need,
                         "pic": item.pic,
                         "admin": admin,
                         "followers": followers,
                         "follower": follower,
                         "interests": item.interests
                         }})


def formatdata(user, result):
    allrecord = []
    for item in result:
        c = ProjectUser.objects.filter(user=user).filter(project=item)
        admin = False
        follower = False
        for i in c:
            # print(i.user, i.project, i.admin)
            admin = i.admin
            follower = True

        followers = ProjectUser.objects.filter(project=item).count()
        record = setdata(item=item,
                         followers=followers,
                         follower=follower,
                         admin=admin
                         )
        allrecord.append(record)
    return allrecord


class OrganizationType(models.Model):
    organizationtype = models.CharField(verbose_name="Nome", max_length=200)

    def __str__(self):
        return self.organizationtype

    class Meta:
        verbose_name = "Tipo de Organização"
        verbose_name_plural = "Tipos de Organização"
        ordering = ['organizationtype']
        db_table = 'tb_organizationtype'


class ProjectManager(models.Manager):

    def search(self, query, user):
        result = self.get_queryset().filter(
            models.Q(name__icontains=query) | \
            models.Q(resume__icontains=query)
        )
        return formatdata(user=user, result=result)


class Project(models.Model):
    name = models.CharField(verbose_name="Nome", max_length=200)
    slug = models.SlugField('Slug')
    pic = models.ImageField(upload_to='project',
                            verbose_name="Foto",
                            null=True,
                            blank=True)
    isfromanyorganization = models.BooleanField(verbose_name='Esse projeto é parte de uma organização', default=False)
    organization = models.CharField(verbose_name="Organização",
                                    max_length=200,
                                    null=True,
                                    blank=True)

    organizationtype = models.ForeignKey(OrganizationType,
                                         on_delete=models.CASCADE,
                                         related_name="organization_type",
                                         verbose_name="Qual tipo de Organização",
                                         null=True)

    interests = models.ManyToManyField(Interests, verbose_name="Interesses")

    resume = models.TextField(verbose_name="Resumo", null=True)
    misson = models.TextField(verbose_name="Missão", null=True)
    vision = models.TextField(verbose_name="Visão", null=True)
    target = models.TextField(verbose_name="Público", null=True)
    values = models.TextField(verbose_name="Precisamos", null=True)
    need = models.TextField(verbose_name="Precisamos", null=True)

    site = models.CharField(verbose_name="Site", max_length=200, null=True)
    blog = models.CharField(verbose_name="Blog", max_length=200, null=True)
    facebook = models.CharField(verbose_name="Facebook", max_length=300, null=True)
    twitter = models.CharField(verbose_name="Twitter", max_length=300, null=True)
    linkedin = models.CharField(verbose_name="Linkedin", max_length=300, null=True)
    status = models.BooleanField(verbose_name='Status', default=True)

    user = models.ManyToManyField(User, verbose_name="Usuário", default=1)

    creator = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Criador", related_name="Project_user",
                                default=1)
    created_at = models.DateTimeField(verbose_name='Criado em', auto_now=True)
    updated_at = models.DateTimeField(verbose_name='Atualizado em', auto_now=True)

    object = ProjectManager()
    objects = models.Manager()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Projeto"
        verbose_name_plural = "Projetos"
        ordering = ['name']
        db_table = 'tb_project'


class ProjectUser(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Usuário", related_name="Project_user_basic",
                             default=1)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name="Projeto",
                                related_name="Project_user_basic",
                                default=1)
    admin = models.BooleanField(verbose_name='Admin', default=False)

    object = ProjectManager()
    objects = models.Manager()

    def __str__(self):
        return self.project

    class Meta:
        verbose_name = "Usuário"
        verbose_name_plural = "Usuários"
        ordering = ['project']
        db_table = 'tb_project_user_basic'
