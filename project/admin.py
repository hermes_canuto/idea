from django.contrib import admin

from project.models import Project, OrganizationType, ProjectUser


# Register your models here.


class ProjectAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'created_at']
    list_display_links = ('id', 'name', 'created_at',)
    search_fields = ['name', 'resume', 'values', 'vision', 'need', 'target']


admin.site.register(Project, ProjectAdmin)


class OrganizationTypeAdmin(admin.ModelAdmin):
    list_display = ['id', 'organizationtype']


admin.site.register(OrganizationType, OrganizationTypeAdmin)


class ProjectUserAdmin(admin.ModelAdmin):
    list_display = ['id', 'user','project']


admin.site.register(ProjectUser, ProjectUserAdmin)
