from django.contrib.auth import authenticate
from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from django.core.validators import validate_email
from django.shortcuts import redirect
from django.shortcuts import render
from django.template.defaultfilters import slugify
from django.utils.html import strip_tags

from core.lib import md5,cleanpost
from core.mail import send_mail_template
from ideia.decorators import islogado
from user.models import User as Usuario, Education, Deficiency, Interests, TrainingArea, level


def register(request):
    template_name = 'register.html'
    ctx = {
        'register': 'class="active"'
    }

    if request.method == "POST":
        nome = request.POST['nome']
        senha = request.POST['senha']
        senha_limpa = request.POST['senha']
        nomeprofile = slugify(nome)
        senha = md5(value=senha)
        email = request.POST['email']

        banco = list(Usuario.objects.filter(email=email))
        if len(banco) != 0:
            ctx['sub_titulo'] = "Registrar"
            ctx['mensagem'] = "Email Já existe na base de dados"
            template_name = "display-answer.html"
        else:
            try:
                # loginuser = User.objects.create_user(email, email, senha_limpa)
                # loginuser.first_name = nome.split()[0]
                # loginuser.groups.set([1])
                # loginuser.save()

                validate_email(email)
                sv = Usuario(name=nome,
                             email=email,
                             password=senha,
                             profile_name=nomeprofile
                             )
                sv.save()
                send_mail_template(context=ctx,
                                   subject="Cadastro IDEIA",
                                   template_name="email-boas-vindas.html",
                                   recipient_list=[request.POST['email']]
                                   )
                ctx['sub_titulo'] = "Registrar"
                ctx['mensagem'] = "Obrigado por registrar em nosso sistema"
                template_name = 'display-answer.html'
            except:
                ctx['sub_titulo'] = "Registrar"
                ctx['mensagem'] = "Email inválido"
                template_name = 'display-answer.html'

    return render(request, template_name, ctx)


def login(request):
    template_name = 'login.html'
    ctx = dict({
        'login': 'class="active"'
    })
    if request.method == "POST":
        senha = request.POST['senha']
        senhalimpa = request.POST['senha']
        senhamd5 = md5(value=senha)
        email = request.POST['email']
        rt = (Usuario.objects.filter(password=senhamd5).filter(email=email))
        loginuser = authenticate(username=email, password=senhalimpa)
        if loginuser is not None:
            print("=" * 20)
            print("logado")
        else:
            print("=" * 20)
            print("nao logado")

        try:
            usuario = rt[0].id
            request.session['usuario'] = usuario
            ctx['usuario'] = rt[0]
            response = redirect('dashboard')
            return response
        except:
            ctx['msg'] = "Usuário Não encontrado!";

    return render(request, template_name, ctx)


@islogado
def dashboard(request):
    template_name = 'logado/dashboard.html'
    ctx = {
        'login': 'class="active"'
    }
    id = request.session['usuario']
    rt = list(Usuario.objects.filter(id=id))
    ctx['usuario'] = rt[0]
    return render(request, template_name, ctx)


@islogado
def perfil(request):
    template_name = 'logado/perfil.html'
    ctx = {
        'login': 'class="active"'
    }
    id = request.session['usuario']
    u = Usuario.objects.get(id=id)

    cleanpost(request)

    if request.method == "POST":
        u.name = request.POST['nome']
        interesses = request.POST.getlist('interesses[]')
        u.Interests.set(interesses)
        trainingarea = request.POST.getlist('trainingarea[]')
        u.trainingarea.set(trainingarea)
        deficiencia = request.POST.getlist('deficiencia[]')
        u.deficiency.set(deficiencia)
        u.facebook = request.POST['facebook']
        u.twitter = request.POST['twitter']
        u.linkedin = request.POST['linkedin']
        u.education = Education.objects.get(id=request.POST['formacao'])
        u.english = level.objects.get(id=request.POST['english'])
        u.Spanish = level.objects.get(id=request.POST['Spanish'])
        u.french = level.objects.get(id=request.POST['french'])
        u.save()

    ctx['educationlist'] = Education.objects.all()
    ctx['deficiencyList'] = Deficiency.objects.all()
    ctx['interestsList'] = Interests.objects.all().order_by("id")
    ctx['trainingareaList'] = TrainingArea.objects.all()
    ctx['levelList'] = level.objects.all()

    ctx['deficiency'] = list(u.deficiency.all())
    ctx['interests'] = list(u.Interests.all())
    ctx['trainingarea'] = list(u.trainingarea.all())
    ctx['usuario'] = u

    return render(request, template_name, ctx)


def logout_view(request):
    logout(request)
    response = redirect('login')
    return response


@islogado
def simple_upload(request):
    id = request.session['usuario']
    u = Usuario.objects.get(id=id)
    ctx = {"usuario": u,
           "pic": u.pic,
           }
    if request.method == 'POST' and bool(request.FILES.get('myfile')) == True:
        myfile = request.FILES['myfile']
        fs = FileSystemStorage()
        filename = fs.save(slugify(myfile.name), myfile)
        uploaded_file_url = fs.url(filename)
        ctx['uploaded_file_url'] = uploaded_file_url.split('/')[2]
    elif request.method == 'POST' and bool(request.POST.get('foto')) == True:
        u.pic = request.POST['foto']
        u.save()
        response = redirect('perfil')
        return response

    return render(request, 'logado/upload.html', ctx)


@islogado
def getuser(request):
    id = request.session['usuario']
    u = Usuario.objects.get(id=id)
    return u
