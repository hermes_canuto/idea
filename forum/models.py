from django.db import models

from project.models import Project
from user.models import User


# Create your models here.

class Topic(models.Model):
    creator = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Criador", related_name="topic_creator")
    project = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name="Projeto", related_name="topic_project")
    title = models.CharField(verbose_name="Título", max_length=200)
    description = models.TextField(verbose_name="Descrição", null=True)
    created_at = models.DateTimeField(verbose_name='Criado em', auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Tópico"
        verbose_name_plural = "Tópicos"
        ordering = ['created_at']
        db_table = 'tb_topic'


class ReplyPost(models.Model):
    title = models.CharField(verbose_name="Título", max_length=200)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE, verbose_name="Topico", related_name="topic_reply")
    creator = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Criador", related_name="replay_creator")
    project = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name="Projeto", related_name="reply_project")
    description = models.TextField(verbose_name="Descrição", null=True)
    created_at = models.DateTimeField(verbose_name='Criado em', auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Respostas para Tópico"
        verbose_name_plural = "Resposta para tópicos"
        ordering = ['created_at']
        db_table = 'tb_reply'
