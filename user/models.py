from django.db import models
from django.utils.html import format_html


class Education(models.Model):
    education = models.CharField(verbose_name="Nome", max_length=200)

    def __str__(self):
        return self.education

    class Meta:
        verbose_name = "Educação"
        verbose_name_plural = "Educação"
        ordering = ['education']
        db_table = 'tb_education'


class Deficiency(models.Model):
    deficiency = models.CharField(verbose_name="Deficiência", max_length=200)

    def __str__(self):
        return self.deficiency

    class Meta:
        verbose_name = "Deficiência?"
        verbose_name_plural = "Deficiências"
        ordering = ['deficiency']
        db_table = 'tb_deficiency'


class Interests(models.Model):
    interests = models.CharField(verbose_name="Interesse", max_length=200)

    def __str__(self):
        return self.interests

    class Meta:
        verbose_name = "Interesse"
        verbose_name_plural = "Interesses"
        ordering = ['interests']
        db_table = 'tb_interests'


class TrainingArea(models.Model):
    trainingarea = models.CharField(verbose_name="Área de Formação", max_length=200)

    def __str__(self):
        return self.trainingarea

    class Meta:
        verbose_name = "Área de Formação"
        verbose_name_plural = "Área de Formação"
        ordering = ['trainingarea']
        db_table = 'tb_trainingarea'


class level(models.Model):
    level = models.CharField(verbose_name="Nivel", max_length=20)

    def __str__(self):
        return self.level

    class Meta:
        verbose_name = "Nivel Línguagem"
        verbose_name_plural = "Nivel Línguagem"
        ordering = ['level']
        db_table = 'tb_level'


class User(models.Model):
    name = models.CharField(verbose_name="Nome", max_length=200)
    profile_name = models.SlugField('Nome do Perfil')
    email = models.EmailField(verbose_name="Email", max_length=200)
    pic = models.ImageField(upload_to='user',
                            verbose_name="Foto",
                            null=True,
                            blank=True)
    password = models.CharField(verbose_name="Senha", max_length=50, null=True, blank=True)
    facebook = models.CharField(verbose_name="Facebook", max_length=300, null=True, blank=True)
    twitter = models.CharField(verbose_name="Twitter", max_length=300, null=True, blank=True)
    linkedin = models.CharField(verbose_name="Linkedin", max_length=300, null=True, blank=True)

    education = models.ForeignKey(Education, on_delete=models.CASCADE, related_name="formacao", verbose_name="Formação",
                                  default=1)
    english = models.ForeignKey(level, on_delete=models.CASCADE, related_name="nivel_ingles", verbose_name="Inglês",
                                default=1)
    Spanish = models.ForeignKey(level, on_delete=models.CASCADE, related_name="nivel_frances", verbose_name="Espanhol",
                                default=1)
    french = models.ForeignKey(level, on_delete=models.CASCADE, related_name="nivel_espanhol", verbose_name="Frances",
                               default=1)

    deficiency = models.ManyToManyField(Deficiency, verbose_name="Deficiencia")
    Interests = models.ManyToManyField(Interests, verbose_name="Interesses")
    trainingarea = models.ManyToManyField(TrainingArea, verbose_name="Área de Formação")

    status = models.BooleanField(verbose_name='Status', default=True)
    created_at = models.DateTimeField(verbose_name='Criado em', auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name='Atualizado em', auto_now=True)

    def thumbnail(self):
        try:
            return format_html('<img src="{}" style="height:100px;"  title="thumbnail"/>'.format(self.pic.url))
        except:
            return format_html('<img src="https://via.placeholder.com/100x100" style="height:100px;"/>')

    thumbnail.short_description = 'Thumbnail'

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Usuário"
        verbose_name_plural = "Usuários"
        ordering = ['id', 'name']
        db_table = 'tb_user'
