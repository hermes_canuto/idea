from django import forms


class Contact(forms.Form):
    CHOICES = (
        ('Propostas', 'Propostas'),
        ('Parceria', 'Parceria'),
        ('Sugestões', 'Sugestões'),
        ('Critica', 'Critica'),
        ('Reportar Irregularidades', 'Reportar Irregularidades'),
        ('Bugs', 'Bugs'),
        ('Outros', 'Outros'),
    )

    name = forms.CharField(label='Nome', max_length=150,widget=forms.TextInput(attrs={'class': 'special'}))
    email = forms.EmailField(label='E-mail', max_length=150,widget=forms.TextInput(attrs={'class': 'special'}))
    select = forms.ChoiceField(label="Assunto", widget=forms.Select, choices=CHOICES(attrs={'class': 'special'}),)
    message = forms.CharField(label='Mensagem/Dúvida', widget=forms.Textarea(attrs={'class': 'special'}))
