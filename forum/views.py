from django.core.paginator import Paginator
from django.shortcuts import redirect
from django.shortcuts import render
from django.urls import resolve
from django.utils.html import strip_tags

from core.lib import cleanpost
from forum.models import Topic, ReplyPost
from project.models import Project
from user.views import getuser


# Create your views here.

def list(request, id_projeto=None):
    u = getuser(request)
    alltopic = Topic.objects.filter(project=id_projeto).reverse();
    paginator = Paginator(alltopic, 3)
    pa = 1
    if request.method == 'GET' and 'pa' in request.GET:
        pa = request.GET['pa']

    if paginator.count > 0:
        paginacao = paginator.num_pages
    else:
        paginacao = False

    ctx = dict({
        "topiclist": paginator.page(pa).object_list,
        "usuario": u,
        "id_project": id_projeto,
        "project": Project.objects.get(id=id_projeto),
        "paginacao": paginacao,
        'paginaatual': pa,
        'buscaurl': "/{}/{}?".format(resolve(request.path_info).url_name, id_projeto),
        'paginarange': paginator.page_range,
    })
    template_name = 'logado/lista-forum.html'
    return render(request, template_name, ctx)


def createtopic(request, id_projeto=None):
    # cleanpost(request)
    u = getuser(request)

    ctx = dict({
        "usuario": u,
        "id_project": id_projeto
    })
    u = getuser(request)

    if request.method == "POST":
        description = request.POST['replaypost']
        topic = Topic(
            project=Project.objects.get(id=id_projeto),
            creator=u,
            title=strip_tags(request.POST['title']),
            description=strip_tags(description)
        )
        topic.save()
        response = redirect("/forum/{}".format(id_projeto))
        return response

    template_name = 'logado/create-forum.html'
    return render(request, template_name, ctx)


def replytopic(request, id_topico=None):
    cleanpost(request)
    u = getuser(request)
    topic = Topic.objects.get(id=id_topico)
    allreply = ReplyPost.objects.filter(topic=topic)

    paginator = Paginator(allreply, 3)
    pa = 1
    if request.method == 'GET' and 'pa' in request.GET:
        pa = request.GET['pa']

    if paginator.count > 0:
        paginacao = paginator.num_pages
    else:
        paginacao = False

    ctx = dict({
        "usuario": u,
        "id_project": topic.project.id,
        "project": Project.objects.get(id=topic.project.id),
        "topic": topic,
        "replylist": paginator.page(pa).object_list,
        "paginacao": paginacao,
        'paginaatual': pa,
        'buscaurl': "/forum/replaytopic/{}?".format(id_topico),
        'paginarange': paginator.page_range,
    })

    if request.method == "POST":
        print("id_project", topic.project.id)
        print(topic)

        reply = ReplyPost(
            project=Project.objects.get(id=topic.project.id),
            creator=u,
            topic=topic,
            title="Respostas de {}".format(u.name),
            description=request.POST['replay'],
        )
        reply.save()
        if paginacao == False:
            paginacao = 1
        response = redirect("/forum/replaytopic/{}?pa={}".format(topic.id, paginacao))
        return response

    template_name = 'logado/create-replay.html'
    return render(request, template_name, ctx)
