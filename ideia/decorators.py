from django.shortcuts import redirect


def islogado(function):
    def wrapper(request, *args, **kwargs):
        print("Verificando sessão")
        print(request.session['usuario'])
        if request.session.is_empty():
            response = redirect('login')
            return response
        return function(request, *args, **kwargs)

    return wrapper
