from django.core.paginator import Paginator
from django.shortcuts import render

from core.models import Contato
from ideia.decorators import islogado
from project.models import ProjectUser, Project
from user.models import User
from user.views import getuser


# Create your views here.


def home(request):
    ctx = {
        'titulo': "HOME",
        'nome': "Nome do Usuário"
    }

    # send_mail_template(context=ctx,
    #                    subject="Teste  de envio com o sendgrid",
    #                    recipient_list=['hermes.canuto@gmail.com'],
    #                    template_name="email-boas-vindas.html"
    #                    )
    return render(request, 'home.html', context=ctx)


def contact(request):
    template_name = 'contact.html'
    ctx = {
        'contact': 'class="active"'
    }

    if request.method == "POST":
        email = request.POST['email']
        nome = request.POST['nome']
        assunto = request.POST['assunto']
        mensagem = request.POST['mensagem']
        ct = Contato(name=nome,
                     email=email,
                     assunto=assunto,
                     mensagem=mensagem)
        ct.save()

        print("Recebendo post")
        print(request.POST)

    return render(request, template_name, context=ctx)


def content(request, tag):
    template_name = 'content.html'
    if tag == 'about':
        ctx = {'about': 'class="active"', 'content': 'aboutus.html'}
    elif tag == 'projects':
        ctx = {'projects': 'class="active"', 'content': 'projetos.html'}
    elif tag == 'partners':
        ctx = {'partners': 'class="active"', 'content': 'parceiros.html'}
    elif tag == 'doit':
        ctx = {'doit': 'class="active"', 'content': 'contribua.html'}

    return render(request, template_name, context=ctx)




@islogado
def buscaall(request):
    if request.method == 'GET':
        busca = request.GET['bsc']
        pa = request.GET['pa']
    elif request.method == 'POST':
        busca = request.POST['bsc']
        pa = 1

    u = getuser(request)
    resultadobusca = (Project.object.search(busca, u))
    paginator = Paginator(list(resultadobusca), 3)
    print("paginas", paginator.count)
    if paginator.count > 0:
        paginacao = paginator.num_pages
    else:
        paginacao = False
    ctx = dict({
        'projeto': 'class="active"',
        'myprojectList': paginator.page(pa).object_list,
        'usuario': u,
        'procurandopor': busca,
        'paginacao': paginacao,
        'paginaatual': pa,
        'paginarange': paginator.page_range,
        'busca': busca,
        'buscaurl': "/busca/?",
        'totalregistro': paginator.count
    })
    template_name = 'logado/lista-projeto.html'
    return render(request, template_name, ctx)





