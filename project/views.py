from django.core.files.storage import FileSystemStorage
from django.core.paginator import Paginator
from django.http import HttpResponse
from django.shortcuts import redirect
from django.shortcuts import render
from django.template.defaultfilters import slugify
from django.utils.html import strip_tags

from ideia.decorators import islogado
from project.models import Project, OrganizationType, ProjectUser, formatdata
from user.models import Interests, User
from user.views import getuser
from core.lib import cleanpost


# Create your views here.

@islogado
def crudprojeto(request, id_projeto=None, acao=None):

    cleanpost(request)

    id = request.session['usuario']
    u = User.objects.get(id=id)
    template_name = 'logado/projeto.html'
    ctx = dict({
        'projeto': 'class="active"',
        'usuario': u
    })
    if acao == 1:
        ctx['acao'] = 1

    if id_projeto is not None:
        try:
            a = ProjectUser.object.filter(user=u).filter(project=id_projeto).count()
            if a != 1:
                ctx['sub_titulo'] = "Erro:"
                ctx['mensagem'] = "Projeto não está disponível para você", id_projeto
                template_name = 'display-answer.html'
                return render(request, template_name, ctx)
            p = Project.objects.get(id=id_projeto)
            ctx['projeto'] = p

        except Project.DoesNotExist:
            ctx['sub_titulo'] = "Erro:"
            ctx['mensagem'] = "Projeto não está disponível ", id_projeto
            template_name = 'display-answer.html'
            return render(request, template_name, ctx)
    else:
        if request.method == "POST":
            if 'id_projeto' in request.POST.keys() and request.POST['id_projeto']:
                p = Project(id=request.POST['id_projeto'])
                print(p.creator.id)
            else:
                p = Project(name=request.POST['nome'])
                p.creator = u
                p.save()
                pu = ProjectUser(user=u, project=p, admin=True)
                pu.save()

    if request.method == "POST":
        p.name = request.POST['nome']
        p.slug = slugify(request.POST['nome'])
        p.resume = request.POST['resumo']
        p.misson = request.POST['missao']
        p.vision = request.POST['visao']
        p.values = request.POST['valores']
        p.target = request.POST['publico']
        p.need = request.POST['precisamos']
        p.site = request.POST['site']
        p.blog = request.POST['blog']
        p.facebook = request.POST['facebook']
        p.twitter = request.POST['twitter']
        p.linkedin = request.POST['linkedin']

        if p.user.filter(id=id):
            print("usuario já existe no projeto")
        else:
            p.user.add(u)

        interesses = request.POST.getlist('interesses[]')
        p.interests.set(interesses)

        p.isfromanyorganization = request.POST['parte']
        a = request.POST['parte']
        if a == '1':
            p.organizationtype = OrganizationType.objects.get(id=request.POST['tipo_organizacao'])
            p.organization = request.POST['qual_organizacao']
        else:
            p.organization = ""
            p.isfromanyorganization = 0
        p.save()

        ctx['projeto'] = p
        response = redirect('/projeto/editar/{}/1'.format(p.id))
        return response

    ctx['interestsList'] = Interests.objects.all().order_by("id")
    ctx['organizationList'] = OrganizationType.objects.all().order_by("id")
    # ctx['interests'] = list(p.interests.all())

    return render(request, template_name, ctx)


@islogado
def meusprojetos(request):
    pa = 1
    if request.method == 'GET' and 'pa' in request.GET:
        pa = request.GET['pa']
    busca = None
    u = getuser(request)
    result = ProjectUser.objects.filter(user=getuser(request))
    allrecord = []
    for item in result:
        # print(item.user)
        allrecord.append(item.project)
    g = formatdata(u, allrecord)
    paginator = Paginator(g, 3)

    if paginator.count > 0:
        paginacao = paginator.num_pages
    else:
        paginacao = False

    ctx = dict({
        'projeto': 'class="active"',
        'myprojectList': paginator.page(pa).object_list,
        'usuario': u,
        'procurandopor': busca,
        'paginacao': paginacao,
        'paginaatual': pa,
        'paginarange': paginator.page_range,
        'busca': busca,
        'buscaurl': "/projeto/listar?",
        'totalregistro': paginator.count
    })
    template_name = 'logado/lista-projeto.html'
    return render(request, template_name, ctx)


@islogado
def participar(request, id_projeto=None):
    u = getuser(request)
    p = ProjectUser.object.filter(project=id_projeto).filter(user=u)
    for i in p:
        if i.admin == 1:
            return HttpResponse("Você é admin não pode sair do projeto")
        else:
            p.delete()
            return HttpResponse("Participar")
    sv = ProjectUser(
        user=u,
        project_id=id_projeto
    ).save()
    return HttpResponse("Deixar de participar")


@islogado
def simple_upload(request, id_projeto=None):
    u = getuser(request)
    p = Project.objects.get(id=id_projeto)
    ctx = {"usuario": u,
           "projeto": p,
           "pic": p.pic,
           "label": "Upload Foto"
           }
    if request.method == 'POST' and bool(request.FILES.get('myfile')) == True:
        myfile = request.FILES['myfile']
        fs = FileSystemStorage()
        filename = fs.save(slugify(myfile.name), myfile)
        uploaded_file_url = fs.url(filename)
        ctx['uploaded_file_url'] = uploaded_file_url.split('/')[2]
    elif request.method == 'POST' and bool(request.POST.get('foto')) == True:
        p.pic = request.POST['foto']
        p.save()
        response = redirect('/projeto/editar/{}'.format(p.id))
        return response

    return render(request, 'logado/upload.html', ctx)


@islogado
def getprojeto(request, id_projeto=None):
    projeto = Project.objects.get(id=id_projeto)
    return projeto
