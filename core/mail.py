import sendgrid
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.defaultfilters import striptags
from django.template.loader import render_to_string
from sendgrid.helpers.mail import *


def sendmailtemplate():
    sg = sendgrid.SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)
    from_email = Email("test@example.com")
    to_email = Email("hermes.canuto@gmail.com")
    subject = "Sending with SendGrid is Fun"
    content = Content("text/plain", "and easy to do anywhere, even with Python")

    mail = Mail(from_email=from_email,
                subject=subject,
                to_email=to_email,
                content=content)
    response = sg.client.mail.send.post(request_body=mail.get())
    print(response.status_code)
    print(response.body)
    print(response.headers)


def send_mail_template(subject,
                       template_name,
                       context,
                       recipient_list,
                       from_email=settings.DEFAULT_FROM_EMAIL,
                       fail_silently=False):
    message_html = render_to_string(template_name, context)
    message_txt = striptags(message_html)
    email = EmailMultiAlternatives(subject=subject,
                                   body=message_txt,
                                   from_email=from_email,
                                   to=recipient_list
                                   )
    email.attach_alternative(message_html, "text/html")
    email.send(fail_silently=fail_silently)
