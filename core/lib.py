# funcoes comuns

import hashlib

from django.utils.html import strip_tags


def md5(value):
    return hashlib.md5(value.encode('utf-8')).hexdigest()


def cleanpost(request):
    request.POST = request.POST.copy()
    print(request.POST)
    chk = ('interesses[]', 'trainingarea[]', 'deficiencia[]')
    for item in request.POST.dict():
        if item not in chk:
            request.POST[item] = strip_tags(request.POST[item])
    print(request.POST)
    return True
