"""ideia URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

import core.views as c
import forum.views as f
import message.views as m
import project.views as p
import user.views as u

admin.site.site_header = 'ADM IDEIA'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', c.home, name="home"),
    path('sobre-nos', c.content, {'tag': 'about'}, name="sobre-nos"),
    path('projetos', c.content, {'tag': 'projects'}, name="projetos"),
    path('colabore', c.content, {'tag': 'doit'}, name="colabore"),
    path('parceiros', c.content, {'tag': 'partners'}, name="parceiros"),
    path('contato', c.contact, name="contato"),

    path('registrar', u.register, name="registrar"),
    path('entrar', u.login, name="login"),
    path('logout', u.logout_view, name="logout"),
    path('dashboard', u.dashboard, name="dashboard"),
    path('perfil', u.perfil, name="perfil"),

    path('projeto', p.crudprojeto, name="projeto"),
    path('projeto/editar/<int:id_projeto>', p.crudprojeto, name="editarprojeto"),
    path('projeto/editar/<int:id_projeto>/<int:acao>', p.crudprojeto, name="editarprojeto"),
    path('projeto/listar/', p.meusprojetos, name="meusprojetos"),
    path('projeto/participar/<int:id_projeto>', p.participar, name="participarprojeto"),
    path('projeto/uploadfoto/<int:id_projeto>', p.simple_upload, name="uploadprojeto"),

    path('upload', u.simple_upload, name="upload"),

    path('message/<int:id_projeto>', m.mensagem, name="mensagem"),

    path('message/send', m.sendmsg, name="sendmessage"),

    path('forum/<int:id_projeto>', f.list, name="forum"),
    path('forum/createtopic/<int:id_projeto>', f.createtopic, name="createtopic"),
    path('forum/replaytopic/<int:id_topico>', f.replytopic, name="createrplaytopic"),

    path('busca/', c.buscaall, name="busca"),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
