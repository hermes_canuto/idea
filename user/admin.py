from django.contrib import admin

from user.models import User, level, Interests, TrainingArea, Deficiency, Education


# Register your models here.


class UserAdmin(admin.ModelAdmin):
    list_display = ['id', 'thumbnail', 'name', 'email', 'created_at']
    list_display_links = ('id', 'thumbnail', 'name', 'email',)
    search_fields = ['name', 'email']
    prepopulated_fields = {'profile_name': ('name',)}
    readonly_fields = ('thumbnail',)


admin.site.register(User, UserAdmin)


class LevelAdmin(admin.ModelAdmin):
    list_display = ['id', 'level']


admin.site.register(level, LevelAdmin)


class InterestsAdmin(admin.ModelAdmin):
    list_display = ['id', 'interests']


admin.site.register(Interests, InterestsAdmin)


class TrainingAreaAdmin(admin.ModelAdmin):
    list_display = ['id', 'trainingarea']


admin.site.register(TrainingArea, TrainingAreaAdmin)


class DeficiencyAdmin(admin.ModelAdmin):
    list_display = ['id', 'deficiency']


admin.site.register(Deficiency, DeficiencyAdmin)


class Educationdmin(admin.ModelAdmin):
    list_display = ['id', 'education']


admin.site.register(Education, Educationdmin)